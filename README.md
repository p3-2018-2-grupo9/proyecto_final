
Descripcion:

En este proyecto base tienen los siguientes directorios

- bin: ubicacion del ejecutable (logdb)
- include: archivos cabecera
    a. Aqui esta la declaracion de estructuras y funciones a implementar.
       SI NECESITAN CREAR MAS ARCHIVOS CABECERA, PUEDEN HACERLO.
    b. Se incluyen los siguientes archivos cabecera:
	 i. logdb.h - interfaz que el cliente usara para conectarse y realizar operaciones en la db. NO CAMBIAR
	ii. utash.h - Una implementacion de una hashtable ligera - NO CAMBIAR.
- src: todos los archivo .c iran aqui
    a. Se ha incluido un programa de prueba para que puedan ver como usar la libreria uthash.
- obj: todos los archivos objetos deben ir a aqui
- lib: librerias (liblogdb.so)
    a. Aqui pondran la implenetacion (como libreria dinamica) de la interfaz (liblogdb.so). NO CAMBIAR EL NOMBRE.
- Makefile (No borrar las reglas adjuntas). Agreguen las reglas necesarias para crear
    a. La libreria liblogdb.so
    b. El ejectuable logdb (servidor de base de datos).
    

El objetivo del proyecto es implementar la base de datos que implica:
	1. El programa logdb, que manejara las solicitudes de los clientes y los archivos de la base de datos.
	2. La libreria liblogdb.so, que permitira a los programas clientes acceder a los servicios ofrecidos por logdb.

NO SE PERMITE MODIFICAR LOS ARCHIVOS include/uthash.h ni include/logdb.h. 

Para descargar, hacer un FORK llamado proyecto_final.


