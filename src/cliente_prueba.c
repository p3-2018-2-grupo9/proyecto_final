#include "logdb.h"
#include "validaciones.h"

int main(int argc, char **argv){

	printf("%s\n", "Usando la funcion conectar db para obtener un puntero a estructura conexionlogdb es decir para iniciar la conexion\n");
	conexionlogdb *con = conectar_db("127.0.0.1",5555);
	
	printf("%s\n","Creando una base de datos\n");
	printf("%d\n",crear_db(con,"sakila2"));

	printf("%s\n", "Abriendo la base de datos\n");
	printf("%d\n",abrir_db(con,"sakila2"));

	printf("%s\n", "Ingresando datos en la base de datos\n");
	printf("%d\n",put_val(con, "mango", "limon"));	
	printf("%d\n",put_val(con, "helado", "frutilla"));	
	printf("%d\n",put_val(con, "frasco", "shampoo"));	
	printf("%d\n",put_val(con, "ducha", "agua"));	
	printf("%d\n",put_val(con, "celular", "contactos"));

	printf("%s\n","mango,limon\nhelado,frutilla\nfrasco,shampoo\nducha,agua\ncelular,contactos\n");

	printf("%s\n", "Eliminando elemento ducha de la base de datos\n");
	printf("%d\n",eliminar(con,"ducha"));

	printf("%s\n", "Extrayendo un valor en base a una clave en la base de datos\n");
	char *valor = get_val(con,"celular");
	if(valor==NULL){
		printf("%s\n","No hay clave para este valor");
	}
	else{
		printf("%s\n",valor);
	}

	valor = get_val(con, "mango");
	if(valor==NULL){
		printf("%s\n","No hay clave para este valor");
	}
	else{
		printf("%s\n",valor);
	}
	
	printf("%s\n", "Ingresando un nuevo valor a la clave existente de la base de datos\n");
	printf("%d\n",put_val(con, "mango", "sal"));

	valor = get_val(con, "mango");
	if(valor==NULL){
		printf("%s\n","No existe la clave en la bd\n");
	}
	else{
		printf("mango con %s\n",valor);
	}
	

	printf("%s\n", "Cerrando la conexion a la base de datos\n");
	cerrar_db(con);

	return 0;
}