#include "validaciones.h"
#include "servidor.h"

int crear_base_servidor(char *archivo){
//	printf("Estoy en crear_base_servidor! :D\n");

	int fd = open(archivo, O_RDONLY, 0);
	if(fd>0){
		close(fd);
		return 0;
	}

	fd = open(archivo,O_WRONLY | O_CREAT, DEF_MODE);
	if(fd>0){

		close(fd);
		return 1;
	}
	
	return 0;
}

base_servidor *abrir_base_servidor(char *archivo_base, char *indice_base){
//	printf("Estoy en abrir_base_servidor! :D\n");

	FILE *f = fopen(archivo_base, "a+");
	if(f==NULL){

		return NULL;
	}
	
	base_servidor *db = (base_servidor *)malloc(sizeof(base_servidor));
	db->nombre_base = strdup(archivo_base);
	if(db->nombre_base == NULL ){
		return NULL;
	}
	db->indice_base = strdup(indice_base);
	if(db->indice_base==NULL){
		return NULL;
	}

	db->f=f;

	sem_init(&db->semaforo,0,1);

    
	return db;
}

int put_servidor(base_servidor *db, char *clave, char *valor){
//	printf("Estoy en set servidor! :D\n");
	if(db==NULL || !validar_char(clave)){

		return 0;
	}
	
	char linea[MAXLINE];
	sprintf(linea, "%s:%s\n", clave, valor);

	fputs(linea, db->f);
	return 1;
	
}

char *get_servidor(base_servidor *db, char *clave){
//	printf("Estoy en get servidor! :D\n");
	if(db==NULL || !validar_char(clave)){
		printf("%s\n","Aqui" );
		
		return NULL;
	}

	FILE* archivo;
	char *linea="";
	char buffer[MAXLINE];

	char* temp;
	char *clave_linea;
	char *valor_linea;

	archivo = db->f;

	if(archivo== NULL){
		printf("No existe el archivo solicitada\n");
		return NULL;
	}

	char *clave_final = (char *)malloc(sizeof(char *));

	fseek(archivo,0,SEEK_SET);
	//printf("Funcion get en el servidor para %s\n\n" ,clave);

	while(linea!=NULL){
		linea = fgets(buffer,MAXLINE,archivo);

		if(linea!=NULL){

			temp = linea;
//			printf("%s en la linea %ld\n",linea ,ftell(archivo));
			clave_linea = strsep(&temp,":");
			valor_linea = strsep(&temp,":");
			printf("Clave linea: %s:Valor linea: %s\n", clave_linea,valor_linea);

			reemplazar(valor_linea,'\n','\0');
			printf("%s comparador %s\n", clave_linea,clave);
			if(strcmp(clave_linea,clave)==0){
				strcpy(clave_final,valor_linea);
				printf("Actualize mi clave final ahora es %s\n",clave_final);			
			}
		}

	}
	printf("Termine y voy a retornar %s\n", clave_final);
	return clave_final;
}


int cerrar_conexion_servidor(base_servidor *db){
//printf("Estoy en cerrar_conexion_servidor! :D\n");

	if(db==NULL){
		return 0;
	}

	fclose(db->f);
	free(db);
	return 1;
}
