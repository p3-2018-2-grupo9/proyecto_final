IDIR =./include
CC=gcc
CFLAGS=-I$(IDIR) -Wall

ODIR=obj

LIBS=-lm -lpthread

_OBJ = abrir_db.o cerrar_db.o conectar_db.o crear_db.o eliminar.o get_val.o put_val.o validaciones.o csapp.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

all:prueba bin/logdb  bin/cliente_prueba

$(ODIR)/%.o: src/%.c $(DEPS)
	$(CC) -c -fPIC -o $@ $< $(CFLAGS)

prueba: prueba.o lib/liblogdb.so
	gcc obj/prueba.o -llogdb -Llib/ -o bin/prueba

prueba.o: src/prueba.c
	gcc -Wall -Iinclude/ -c src/prueba.c -o obj/prueba.o

lib/liblogdb.so: $(OBJ)
	$(CC) -shared -fPIC -o $@ $^ $(CFLAGS) $(LIBS)
obj/logdb.o: src/logdb.c
	gcc -Wall -Iinclude src/logdb.c -c -o obj/logdb.o
obj/servidor.o: src/servidor.c
	gcc -Wall -Iinclude src/servidor.c -c -o obj/servidor.o

bin/logdb: obj/logdb.o obj/servidor.o obj/csapp.o obj/validaciones.o
	gcc -Wall -o bin/logdb obj/logdb.o obj/servidor.o obj/validaciones.o obj/csapp.o -I./include $(LIBS) -Llib

bin/cliente_prueba: src/cliente_prueba.c obj/csapp.o lib/liblogdb.so
	gcc -Wall  src/cliente_prueba.c obj/csapp.o -I./include  -lpthread lib/liblogdb.so -L./lib -o $@

.PHONY: clean

clean:
	@if [ `ls bin/ -1A | wc -l` -ne 0 ]; then { rm bin/* obj/* prueba/sakila2_log ; echo "limpiando archivos"; } fi